import React from 'react';
import {
	SafeAreaView,
	View,
	FlatList,
	StyleSheet,
	Image,
	Text,
	TouchableOpacity,
} from 'react-native';
import {getPopularMovies} from '../../redux/movies/actions';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

function Item({item}) {
	let {title, overview, poster_path} = item;
	return (
		<View
			style={{
				padding: 10,
				alignItems: 'center',
			}}>
			<Image
				source={{uri: `http://image.tmdb.org/t/p/w185/${poster_path}`}}
				style={{height: 130, width: 130, borderRadius: 100}}
			/>
			<Text style={{fontWeight: 'bold'}}>{title}</Text>
			<Text>{overview}</Text>
		</View>
	);
}

class PopularMovies extends React.Component {
	state = {
		page: 1,
	};

	componentDidMount() {
		let {page} = this.state;
		this.props.getPopularMovies(page);
	}

	flatListItemSeparator = () => {
		return (
			<View
				style={{
					height: 1,
					width: '100%',
					backgroundColor: '#000',
				}}
			/>
		);
	};

	flatListHeader = () => {
		return (
			<View
				elevation={1}
				style={{
					height: 100,
					width: '97%',
					margin: 5,
					backgroundColor: '#fff',
					border: 2.9,
					borderColor: 'black',
					alignSelf: 'center',
					shadowColor: '#000',
					shadowOffset: {
						width: 0,
						height: 16,
					},
					shadowOpacity: 1,
					shadowRadius: 7.49,
				}}>
				<Text
					style={{
						textShadowColor: 'black',
						textShadowOffset: {width: 1, height: 3},
						textShadowRadius: 10,
						fontSize: 40,
						fontWeight: '800',
						flex: 1,
						alignSelf: 'center',
						paddingTop: 30,
						fontSize: 40,
					}}>
					Popular Movies
				</Text>
			</View>
		);
	};

	flatListFooter = () => {
		return (
			<TouchableOpacity onPress={this.loadMoreMovies}>
				<View
					elevation={1}
					style={{
						height: 100,
						width: '97%',
						margin: 5,
						backgroundColor: '#fff',
						border: 2.9,
						borderColor: 'green',
						alignSelf: 'center',
						shadowColor: '#000',
						shadowOffset: {
							width: 0,
							height: 16,
						},
						shadowOpacity: 1,
						shadowRadius: 7.49,
					}}>
					<Text
						style={{
							textShadowColor: 'green',
							textShadowOffset: {width: 1, height: 3},
							textShadowRadius: 10,
							fontWeight: '800',
							flex: 1,
							alignSelf: 'center',
							paddingTop: 30,
							fontSize: 20,
						}}>
						Load more..
					</Text>
				</View>
			</TouchableOpacity>
		);
	};

	loadMoreMovies = () => {
		let {page} = this.state;
		this.setState(
			{
				page: page + 1,
			},
			() => {
				this.props.getPopularMovies(page);
			},
		);
	};

	render() {
		let {movies} = this.props;
		return (
			<FlatList
				ListHeaderComponent={this.flatListHeader}
				ItemSeparatorComponent={this.flatListItemSeparator}
				ListFooterComponent={this.flatListFooter}
				data={movies}
				renderItem={({item}) => <Item item={item} />}
				keyExtractor={item => item.id}
			/>
		);
	}
}
const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	item: {
		backgroundColor: '#f9c2ff',
		padding: 20,
		marginVertical: 8,
		marginHorizontal: 16,
	},
	title: {
		fontSize: 32,
	},
});

const mapDispatchToProps = dispatch => ({
	getPopularMovies: bindActionCreators(getPopularMovies, dispatch),
});

const mapStateToProps = state => ({
	movies: state.movies,
});

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(PopularMovies);
