import * as MOVIESTYPE from './types';

let initialState = {
  movies: [],
};
const movies = (state = initialState, action) => {
  switch (action.type) {
    case MOVIESTYPE.POPULAR_MOVIES_FETCHED:
      state.movies = action.data.results;
      return {...state};

    default:
      return state;
  }
};
export default movies;
