import * as MOVIESTYPE from './types';

export const popularMoviesFetched = data => ({
	type: MOVIESTYPE.POPULAR_MOVIES_FETCHED,
	data,
});

export const getPopularMovies = page => {
	return dispatch => {
		fetch(
			`https://api.themoviedb.org/3/movie/popular?api_key=289f59f2c49c8b19ffd130669d32b811&language=en-US&page=${page}`,
		)
			.then(res => res.json())
			.then(resultJson => dispatch(popularMoviesFetched(resultJson)))
			.catch(e => console.log(e, 'error'));
	};
};
