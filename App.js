/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import {PersistGate} from 'redux-persist/integration/react';
import movies from './src/redux/movies';
import configureStore from './src/config/configureStore';
import PopularMovies from './src/screens/popularMovies';
const {store, persistor} = configureStore();

const App = () => {
	return (
		<Provider store={store}>
			<PersistGate loading={null} persistor={persistor}>
				<PopularMovies />
			</PersistGate>
		</Provider>
	);
};

export default App;
